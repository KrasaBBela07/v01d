const DELAY = 7
const BALL_DEFAULT_X = 150
const BALL_DEFAULT_Y = 150
const KEY_CODE_RIGHT = "KeyD"
const KEY_CODE_LEFT = "KeyA"

const canvas = document.querySelector("#gamewindow")
const ctx = canvas.getContext("2d")

let leftPressed = false
let rightPressed = false
let bricks = []



const ball = {
    x: BALL_DEFAULT_X,
    y: BALL_DEFAULT_Y,
    dx: 4,
    dy: 4,
    radius: 15,
    color: "red",
}

const paddle = {
    width: 200,
    height: 15,
    offsetBottom: 1,
    dx: 7,
    color: "Blue"
}
paddle.x = canvas.width / 2 - (paddle.width / 2)


const brickInfo = {
    rows: 3,
    cols: 5,
    width: 70,
    height: 20,
    topOffset: 30,
    padding: 15,
    color: "blue",
}
brickInfo.leftOffset = (canvas.width - brickInfo.cols * (brickInfo.padding + brickInfo.width)) / 2


function clearCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height)
}


function drawBall() {
    ctx.beginPath()
    ctx.arc(ball.x, ball.y, ball.radius, 0, 2 * Math.PI)
    ctx.fillStyle = ball.color
    ctx.fill()
    ctx.closePath()
}


function drawPaddle() {
    ctx.beginPath()
    ctx.rect(
        paddle.x,
        canvas.height - paddle.height - paddle.offsetBottom,
        paddle.width,
        paddle.height
    )
    ctx.fillStyle = paddle.color
    ctx.fill()
    ctx.closePath()
}


function drawBricks() {
    for (let i = 0; i < brickInfo.rows; i++) {
        for (let j = 0; j < brickInfo.cols; j++) {
            const brick = bricks[i][j]
            brick.x = brickInfo.leftOffset + (j * (brickInfo.width + brickInfo.padding))
            brick.y = brickInfo.topOffset + (i * (brickInfo.height + brickInfo.padding))

            ctx.beginPath()
            ctx.rect(brick.x, brick.y, brickInfo.width, brickInfo.height)
            ctx.fillStyle = brickInfo.color
            ctx.fill()
            ctx.closePath()
        }
    }
}


function nextTick() {
    if (ball.y + ball.dy < ball.radius) {
        ball.dy = -ball.dy
    } else if (ball.y + ball.dy > canvas.height - ball.radius - paddle.offsetBottom) {
        if (ball.x > paddle.x && ball.x < paddle.x + paddle.width) {
            ball.dy = -ball.dy
        } else {
            clearInterval(gameLoopId)
            alert("Ну ты и лох конечно")
        }
    }

    if (ball.x + ball.dx > canvas.width - ball.radius || ball.x + ball.dx < ball.radius) {
        ball.dx = -ball.dx
    }

    if (leftPressed && paddle.x > 0) {
        paddle.x -= paddle.dx
    }

    if (rightPressed && paddle.x + paddle.width < canvas.width) {
        paddle.x += paddle.dx
    }

    ball.x += ball.dx
    ball.y += ball.dy
}


function draw() {
    clearCanvas()

    drawBricks()
    drawBall()
    drawPaddle()

    nextTick()
}


document.body.onkeydown = (event) => {
    if (event.code === KEY_CODE_LEFT) {
        leftPressed = true
    }

    if (event.code === KEY_CODE_RIGHT) {
        rightPressed = true
    }
}

document.body.onkeyup = (event) => {
    if (event.code === KEY_CODE_LEFT) {
        leftPressed = false
    }

    if (event.code === KEY_CODE_RIGHT) {
        rightPressed = false
    }
}


for (let i = 0; i < brickInfo.rows; i++) {
    bricks[i] = []
    for (let j = 0; j < brickInfo.cols; j++) {
        bricks[i][j] = {
            x: 0,
            y: 0,
        }
    }
}


const gameLoopId = setInterval(draw, DELAY)